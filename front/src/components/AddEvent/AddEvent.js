import React, {Component} from 'react';
import DayPickerInput from 'react-day-picker/DayPickerInput';
import {connect} from 'react-redux';
import 'react-day-picker/lib/style.css';
import {addEvent, getEvent} from "../../store/actions/main";

class AddEvent extends Component {
    state = {
        start: "",
        message: ""

    };
    componentDidMount(){
        this.props.getEvent()
    }

    handleChange = event => {
        this.setState({start: event})
    };
    sendEvent = () => {
        if(!this.state.start||!this.state.message)
            return;
        this.props.createEvent(this.state);
        this.setState({
            start: "",
            message: ""
        });

    };

    render() {
        return (
            <div>
                <DayPickerInput value={this.state.start} onDayChange={this.handleChange}/>
                <input type="text" onChange={(event) => {
                    this.setState({message: event.target.value})
                }} value={this.state.message}/>
                <button onClick={this.sendEvent}>Send</button>
            </div>
        )
    }
};


const mapStateToProps = state => ({});

const mapDispatchToProps = dispatch => ({
    createEvent: data => dispatch(addEvent(data)),
    getEvent:()=>dispatch(getEvent())
});

export default connect(mapStateToProps, mapDispatchToProps)(AddEvent);
