import React, {Component} from 'react';
import 'fullcalendar-reactwrapper/dist/css/fullcalendar.min.css';
import FullCalendar from 'fullcalendar-reactwrapper';
import {connect} from 'react-redux';


class Celendar extends Component {
    state = {
        events: [
            {
                title: 'All Day Event',
                start: '2017-05-01'
            },
            {
                title: 'Long Event',
                start: '2017-05-07',
            },
            {
                id: 999,
                title: 'Repeating Event',
                start: '2017-05-09T16:00:00'
            },
            {
                id: 999,
                title: 'Repeating Event',
                start: '2017-05-16T16:00:00'
            },
            {
                title: 'Conference',
                start: '2018-06-11'
            },
            {
                title: 'Meeting',
                start: '2017-05-12T10:30:00'
            },
            {
                title: 'Birthday Party',
                start: '2017-05-13T07:00:00'
            },
            {
                title: 'Click for Google',
                url: 'http://google.com/',
                start: '2017-05-28'
            }
        ],
    };


    render() {
        console.log(this.props.events);
        return (
            <div id="example-component">
                <button>New Event</button>
                <FullCalendar
                    id="your-custom-ID"
                    header={{
                        left: 'prev,next today myCustomButton',
                        center: 'title',
                        right: 'month,basicWeek,basicDay'
                    }}
                    defaultDate={new Date()}
                    navLinks={true} // can click day/week names to navigate views
                    editable={true}
                    eventLimit={true} // allow "more" link when too many events
                    events={this.props.events}
                />
            </div>
        );
    }


}

const mapStateToProps = state=>({
    events: state.main.events
});
export default connect(mapStateToProps)(Celendar);