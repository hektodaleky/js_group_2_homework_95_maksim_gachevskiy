import {GET_EVENTS_SUCCESS, LOGIN_USER_SUCCESS} from "../actions/actionTypes";

const initialState = {
    registerError: null,
    loginError: null,
    user: null,
    token: null,
    events: []
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case LOGIN_USER_SUCCESS:
            return {...state, token: action.token, user: action.user};
        case GET_EVENTS_SUCCESS:
            return {...state, events:action.events}

        default:
            return state;
    }
};

export default reducer;