import {CREATE_EVENT, GET_EVENTS, GET_EVENTS_SUCCESS, LOGIN_FACEBOOK, LOGIN_USER_SUCCESS} from "./actionTypes";
import {NotificationManager} from "react-notifications";
import {loginUserFailure, loginUserSuccess} from "./users";
import axios from "../../axios-api";
import {push} from "react-router-redux";

export const loginFacebook = userData => {
    return {type: LOGIN_FACEBOOK, userData}
};

export const addEvent = myEvent => {
    return {type: CREATE_EVENT, myEvent}
};

export const getEvent = (events) => {
    return {type: GET_EVENTS, events};
};
export const getEventSuccess = (events) => {
    return {type: GET_EVENTS_SUCCESS, events};
};