import axios from '../../axios-api';
import {NotificationManager} from 'react-notifications';
import {push} from 'react-router-redux';
import {
    LOGIN_USER_FAILURE,
    LOGIN_USER_SUCCESS,
    LOGOUT_USER, REGISTER_USER,
    REGISTER_USER_FAILURE,
    REGISTER_USER_SUCCESS
} from "./actionTypes";

// export const registerUserSuccess = () => {
//     return {type: REGISTER_USER_SUCCESS};
// };
//
// export const registerUserFailure = error => {
//     return {type: REGISTER_USER_FAILURE, error};
// };
//
// export const registerUser=userData=>{
//     return {type: REGISTER_USER,userData}
// };
export const loginUserSuccess = (user, token) => {
    return {type: LOGIN_USER_SUCCESS, user, token};
};

export const loginUserFailure = error => {
    return {type: LOGIN_USER_FAILURE, error};
};


export const facebookLogin = data => {
    return dispatch => {
        axios.post('/users/facebookLogin', data).then(
            response => {
                dispatch(loginUserSuccess(response.data.user, response.data.token));
                dispatch(push('/'));
                NotificationManager.success('Logged in with Facebook!', 'Success');
            },
            error => {
                dispatch(loginUserFailure(error.response.data));
            }
        )
    };
};