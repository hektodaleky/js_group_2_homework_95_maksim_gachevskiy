import {takeEvery} from 'redux-saga/effects'
import {CREATE_EVENT, GET_EVENTS, LOGIN_FACEBOOK} from "../actions/actionTypes";
import {createEvent, getEvent, loginFacebook} from "./main";

export function* watchRegisterUser() {
    yield takeEvery(LOGIN_FACEBOOK, loginFacebook);
    yield takeEvery(CREATE_EVENT, createEvent);
    yield takeEvery(GET_EVENTS,getEvent)
}