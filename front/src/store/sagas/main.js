import axios from "../../axios-api";
import {loginUserFailure, loginUserSuccess} from "../actions/users";
import {put} from 'redux-saga/effects';
import {NotificationManager} from "react-notifications";
import {push} from "react-router-redux";
import {getEventSuccess} from "../actions/main";

export function* loginFacebook(action) {
    try {
        const response = yield axios.post('/users/facebookLogin', action.userData);

        yield put(loginUserSuccess(response.data.user, response.data.token));
        yield put(push('/'));
        yield NotificationManager.success('Logged in with Facebook!', 'Success');

    }
    catch (error) {
        yield put(loginUserFailure(error.response.data));
    }

}

export function* createEvent(action) {
    console.log(action.myEvent);
    try {
         yield axios.post('/events', action.myEvent);

        yield put(push('/'));
        const response = yield axios.get('/events');
        yield put(getEventSuccess(response.data.event));

    }
    catch (error) {
        console.log(error)
    }
}

export function* getEvent(action) {
    try {
        const response = yield axios.get('/events');
        yield put(getEventSuccess(response.data.event));

        yield put(push('/'));

    }
    catch (error) {
        console.log(error)
    }
}
