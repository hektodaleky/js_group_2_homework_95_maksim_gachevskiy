import axios from "../../axios-api";
import {registerUserFailure, registerUserSuccess} from "../actions/users";
import {put} from 'redux-saga/effects';
import {push} from "react-router-redux";
import {NotificationManager} from "react-notifications";

export function* registerUserSaga(action) {
    try {
        const response = yield axios.post('/users', action.userData);
        yield put(registerUserSuccess());
        yield put(push('/'));
        yield NotificationManager.success('Success', 'Registration successful');
        yield NotificationManager.success('Logged in with Facebook!', 'Success');
    }
    catch (error) {
        yield put(registerUserFailure(error.response.data))
    }

}