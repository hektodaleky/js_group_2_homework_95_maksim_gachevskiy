import React, {Component, Fragment} from 'react';
import './App.css';
import FacebookLogin from 'react-facebook-login/dist/facebook-login-render-props';
import config from "./config";
import {Button} from "react-bootstrap";
import {connect} from 'react-redux';
import Celendar from "./containers/Celendar/Celendar";
import {addEvent, getEvent, loginFacebook} from "./store/actions/main";
import AddEvent from "./components/AddEvent/AddEvent";


class App extends Component {
    componentDidUpdate(){
        this.props.getEvent()
    }


    facebookResponse = response => {
        if (response.id) {
            this.props.facebookLogin(response);
        }
        else {

        }
    };

    render() {
        console.log(this.props.user);
        return (
            <div className="App">

                {this.props.user ? <Fragment>
                    <div style={{marginBottom: "150px"}}><AddEvent/></div>
                    <Celendar/></Fragment> :

                    <FacebookLogin
                    appId={config.facebookAppId}
                    fields="name,email,picture"
                    callback={this.facebookResponse}
                    render={renderProps => (

                        <Button onClick={renderProps.onClick}>
                            Sign in with Facebook
                        </Button>
                    )}/>}


            </div>
        );
    }
}

const mapStateToProps = state => ({
    user: state.main.user
});

const mapDispatchToProps = dispatch => ({
    facebookLogin: data => dispatch(loginFacebook(data)),
    getEvent:()=>dispatch(getEvent())
});

export default connect(mapStateToProps, mapDispatchToProps)(App);
