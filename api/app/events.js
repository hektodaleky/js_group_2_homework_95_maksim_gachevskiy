const express = require('express');
const https = require('https');
const request = require('request-promise-native');
const User = require('../models/User');
const Event = require('../models/Events');


const config = require('../config');
const nanoid = require("nanoid");

const createRouter = () => {
    const router = express.Router();


    router.post('/', async (req, res) => {
        const token = req.get('Token');

        if (!token) {
            return res.status(401).send({message: 'Token not present'});
        }
        const user = await User.findOne({token});

        if (!user) {
            return res.status(401).send({message: 'User not found'});
        }


        event = new Event({
            user: user.email,
            title: req.body.message,
            start: req.body.start
        });
        await event.save();


    });

    router.get('/', async (req, res) => {
        const token = req.get('Token');
        if (!token) {
            return res.status(401).send({message: 'Token not present'});
        }
        const user = await User.findOne({token});

        if (!user) {
            return res.status(401).send({message: 'User not found'});
        }

        const events = await Event.find({user: user.email});
        console.log('get', events)
        res.send({event: events})

    });
    return router;
};

module.exports = createRouter;