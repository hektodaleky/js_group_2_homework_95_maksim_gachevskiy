const path = require('path');

const rootPath = __dirname;

module.exports = {
  rootPath,
  uploadPath: path.join(rootPath, '/public/uploads'),
  db: {
    url: 'mongodb://localhost:27017',
    name: 'calendar'
  },
  facebook: {
    appId: "507628979655551", // Enter your app ID here
    appSecret: "a50c783fda5696e515f4165c30376514" // Enter your app secret here
  }
};

