const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const EventsSchema = new Schema({
    user: String,
    title: {
        type: String,
        required: true
    },
    start: {
        type: Date,
        required: true
    }
});

const Events = mongoose.model('Events', EventsSchema);

module.exports = Events;